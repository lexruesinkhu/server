//
// Created by lex on 31-12-17.
//

#ifndef SERVER_CONTROLLER_MANAGER_HPP
#define SERVER_CONTROLLER_MANAGER_HPP

#include <utility>
#include <vector>

#include <boost/asio.hpp>

using boost::asio::ip::tcp;

class controller_manager
{
public:
    /**
     * Add a controller to the manager.
     *
     * @param endpoint
     */
    void add_controller(connection_ptr conn) {
        std::cout << "Controller registered from " << conn->getRemoteAddress().to_string() << std::endl;

        controllers_.push_back(conn);
    }

    std::vector<connection_ptr> &get_controllers() {
        return controllers_;
    }

    void broadcast(std::string const &message) {
        for (auto &controller: controllers_) {
            controller->write(message);
        }
    }

    void remove_if_exists(connection_ptr conn) {
        auto it = std::find(std::begin(controllers_),
                            std::end(controllers_), conn);

        if (it != std::end(controllers_)) {
            controllers_.erase(it);
        }
    }

    /**
     * Get the singleton instance of the controller_manager class.
     *
     * @return
     */
    static controller_manager &get_instance() {
        static controller_manager cm;

        return cm;
    }

    controller_manager(controller_manager const &) = delete;

    void operator=(controller_manager const &) = delete;

private:
    controller_manager() = default;

protected:
    std::vector<connection_ptr> controllers_;
};


#endif //SERVER_CONTROLLER_MANAGER_HPP
