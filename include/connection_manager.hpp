//
// Created by lex on 17-1-18.
//

#ifndef SERVER_CONNECTION_MANAGER_HPP
#define SERVER_CONNECTION_MANAGER_HPP

#include <set>
#include <connection.hpp>

#include <typedefs.hpp>
#include "message_handler.hpp"

class connection_manager
{
public:
    /**
     * Create a new connection.
     *
     * @param io_service
     * @return
     */
    connection_ptr make_new(boost::asio::io_service &io_service) {
        auto conn = std::make_shared<connection>(io_service);

        connections_.insert(conn);

        return conn;
    }

    /**
     * Start reading on the given connection.
     *
     * @param conn
     */
    void start_reading(connection_ptr conn) {
        conn->read([&](connection_ptr conn, std::string data) {
            // std::cout << "start_reading: " << data << std::endl;
            handler_->handle(conn, data);
        });
    }

    /**
     * Remove the given connection from the set of
     * managed connections.
     *
     * @param conn
     */
    void remove(connection_ptr conn) {
        connections_.erase(conn);
    }

    /**
     * Get the connection manager instance.
     *
     * @return
     */
    static connection_manager &get_instance() {
        static connection_manager cm;

        return cm;
    }

    connection_manager(connection_manager const &) = delete;

    void operator=(connection_manager const &) = delete;

private:
    connection_manager() : handler_({}) {}

protected:
    std::set<connection_ptr> connections_;
    std::shared_ptr<message_handler> handler_;
};

#endif //SERVER_CONNECTION_MANAGER_HPP
