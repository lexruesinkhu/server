//
// Created by lex on 21-1-18.
//

#ifndef SERVER_CLI_HPP
#define SERVER_CLI_HPP

#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

#include <boost/algorithm/string/predicate.hpp>
#include <boost/algorithm/string/trim.hpp>
#include <boost/lexical_cast.hpp>

#include <client_manager.hpp>
#include <app_manager.hpp>

class cli
{
public:
    void loop() {
        std::cout << "Now listening for console input..." << std::endl;

        while (!done) {
            std::string input_line;
            std::getline(std::cin, input_line);
            to_lowercase(input_line);
            handle_command(input_line);
        }
    }

private:
    void handle_command(std::string &command) {
        bool found = true;

        if (command == "exit") {
            std::cout << "Exiting..." << std::endl;
            done = true;
        } else if (boost::starts_with(command, "broadcast")) {
            eat_until_space(command);

            client_manager::get_instance().broadcast(command);
            app_manager::get_instance().broadcast(command);

            std::cout << "Broadcast message send." << std::endl;
        } else if (boost::starts_with(command, "client")) {
            eat_until_space(command);

            if (boost::starts_with(command, "list")) {
                auto &clients = client_manager::get_instance().get_clients();

                if (clients.empty()) {
                    std::cout << "There are currently no registered clients." << std::endl;
                } else {
                    for (auto &client : clients) {
                        std::cout << client.id << ": " << client.conn->getRemoteAddress() << "\n";
                    }

                    std::cout << std::endl;
                }
            } else if (boost::starts_with(command, "show")) {
                int id;

                eat_until_space(command);

                try {
                    id = boost::lexical_cast<int>(command);
                } catch (boost::bad_lexical_cast /* ex */) {
                    std::cout << "Id could not be converted to an integer." << std::endl;
                    return;
                }

                auto client = client_manager::get_instance().get_client(id);

                if (!client) {
                    std::cout << "A client with the id " << id << " does not exist." << std::endl;
                } else {
                    std::cout << "Ip: " << (*client).conn->getRemoteAddress() << std::endl;
                }
            } else {
                found = false;
            }
        } else if (boost::starts_with(command, "event")) {
            eat_until_space(command);

            if (boost::starts_with(command, "clear")) {
                database::get_instance().clear_events();
            } else {
                found = false;
            }
        } else {
            found = false;
        }

        if (!found) {
            std::cout << "Command not found." << std::endl;
        }
    }

    void eat_until_space(std::string &input) {
        std::size_t found = input.find(' ');

        if (found != std::string::npos) {
            input = input.substr(found + 1);
        }
    }

    void trim(std::string &input) {
        boost::trim(input);
    }

    void to_lowercase(std::string &text) {
        std::transform(text.begin(), text.end(), text.begin(), ::tolower);
    }

    bool done = false;
};

#endif //SERVER_CLI_HPP
