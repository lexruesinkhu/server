//
// Created by lex on 15-1-18.
//

#ifndef SERVER_DATABASE_HPP
#define SERVER_DATABASE_HPP

#include <string>
#include <vector>
#include "events.hpp"

struct sqlite3;

class database {
public:
    /**
     * Get the database instance.
     *
     * @return
     */
    static database& get_instance();

    /**
     * Load the database from file.
     * The file will be created if needed.
     * If the required tables do not exist, they
     * will be created.
     *
     * @param filename
     * @return
     */
    bool init(const char* filename);

    /**
     * Close the database.
     */
    void close();

    ~database() { close(); }

    database(database const&) = delete;
    void operator=(database const&) = delete;

    /**
     * Send a query to the database. Will use
     * sqlite3_column_text casted to const char* for
     * the result. Will always use prepared statements.
     *
     * @param query
     * @return
     */
    const char* query(std::string const& query);

    /**
     * Insert a event into the database.
     *
     * @param event_type
     * @param room_nr
     */
    void insert_event(event event);

    /**
     * Get events from the database.
     *
     * @return
     */
    std::vector<event> get_events(int amount, int offset = 0);

    /**
     * Clear all events from the database.
     */
    void clear_events();

private:
    database() = default;

protected:
    void bootstrap();
    bool needs_setup();

    sqlite3 *db;
};

#endif //SERVER_DATABASE_HPP
