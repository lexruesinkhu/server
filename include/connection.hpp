//
// Created by lex on 17-1-18.
//

#ifndef SERVER_CONNECTION_HPP
#define SERVER_CONNECTION_HPP

#include <deque>
#include <iostream>

#include <boost/asio.hpp>
#include <boost/bind.hpp>

using boost::asio::ip::tcp;

class connection : public std::enable_shared_from_this<connection>
{
    typedef std::shared_ptr<connection> connection_ptr;
    typedef std::function<void(connection_ptr, std::string)> receive_handler;

public:
    explicit connection(boost::asio::io_service &io_service)
            : socket_(io_service) {
    }

    /**
     * Start reading on the connection.
     *
     * @param on_receive
     */
    void read(receive_handler on_receive);

    /**
     * Write a message to the connection.
     *
     * @param message
     */
    void write(const std::string &message);

    /**
     * Get the remote address associated with
     * the underlying socket.
     *
     * @return
     */
    boost::asio::ip::address getRemoteAddress() {
        return socket_.remote_endpoint().address();
    }

    /**
     * Get a reference to the underlying socket.
     *
     * @return
     */
    tcp::socket &socket() {
        return socket_;
    }

private:
    /**
     * Recursive (infinite) async read handler.
     *
     * @param on_receive
     * @param ec
     */
    void do_read(receive_handler on_receive, boost::system::error_code const &ec = {});

    /**
     * Handle packet that has been received.
     *
     * @param on_receive
     * @param ec
     * @param bytes_received
     */
    void handle_read(receive_handler on_receive, boost::system::error_code const &ec, size_t bytes_received);

    /**
     * Recursive (infinite) async write handler that processes the write_queue.
     */
    void handle_write();

    enum
    {
        buffer_length = 1024
    };

    char read_buffer_[buffer_length];

    tcp::socket socket_;
    std::deque<std::string> write_queue_;
};

#endif //SERVER_CONNECTION_HPP
