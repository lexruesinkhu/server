//
// Created by lex on 23-1-18.
//

#include <boost/asio/ip/tcp.hpp>

#include "connection_manager.hpp"

#ifndef SERVER_APP_MANAGER_HPP
#define SERVER_APP_MANAGER_HPP

using boost::asio::ip::tcp;

class app_manager
{
public:
    void add_app(connection_ptr conn) {
        std::cout << "App registered from " << conn->socket().remote_endpoint().address().to_string() << std::endl;

        apps_.push_back(conn);
    }

    std::vector<connection_ptr> &get_apps() {
        return apps_;
    }

    void broadcast(std::string const &message) {
        for (auto &app : apps_) {
            app->write(message);
        }
    }

    void remove_if_exists(connection_ptr conn) {
        auto it = std::find(std::begin(apps_), std::end(apps_), conn);

        if (it != std::end(apps_)) {
            apps_.erase(it);
        }
    }

    static app_manager &get_instance() {
        static app_manager am;

        return am;
    }

    app_manager(app_manager const &) = delete;

    void operator=(app_manager const &) = delete;

private:
    app_manager() = default;

protected:
    std::vector<connection_ptr> apps_;
};

#endif //SERVER_APP_MANAGER_HPP
