//
// Created by lex on 25-11-17.
//

#ifndef SERVER_SERVER_HPP
#define SERVER_SERVER_HPP

#include <boost/asio.hpp>
#include <connection_manager.hpp>
#include <message_handler.hpp>

using namespace boost;
using boost::asio::ip::tcp;

struct session;

class tcp_server
{
public:
    tcp_server(asio::io_service &io_service, unsigned short port);

private:
    void do_accept();
    void handle_accept(connection_ptr conn, boost::system::error_code const& ec);
    //void handle_accept(session* new_session, const boost::system::error_code& error_code);

    asio::io_service &io_service_;
    tcp::acceptor acceptor_;
};


#endif //SERVER_SERVER_HPP
