//
// Created by lex on 25-11-17.
//

#ifndef SERVER_MESSAGE_HPP
#define SERVER_MESSAGE_HPP

#include <string>
#include <sstream>

#include <boost/asio/ip/tcp.hpp>
#include <boost/fusion/container.hpp>
#include <boost/optional.hpp>
#include <boost/fusion/include/at_c.hpp>

#include <typedefs.hpp>
#include <boost/phoenix/support/vector.hpp>

using namespace boost::asio::ip;

using boost::fusion::at_c;
using output_iterator = std::back_insert_iterator<std::string>;

enum packet_type : int
{
    discovery = 0,
    discovery_answer = 1,
    state_change = 2,
    state_fetch = 3,
    state_response = 4,
    control = 5,
    keepalive = 6,
};

enum state_changes : int
{
    light_turned_on = 0,
    light_turned_off = 1,
    camera_enabled = 2,
    camera_disabled = 3,
    start_emergency = 4
};

enum state_fetches : int
{
    get_clients = 0,
    get_events = 1,
    get_ip = 2
};

enum control_types : int
{
    get_id = 0,
    register_controller = 1,
    turn_light_on = 2,
    register_app = 3,
    turn_light_off = 4
};


// UDP packets
struct discovery_packet
{
};

struct discovery_answer_packet
{
};

// TCP packets
struct base_packet
{
    connection_ptr conn;

    std::string conversation_id;

    int source_id;
    int destination_id;

    explicit base_packet(connection_ptr conn)
            : conn(conn) {
    }

    explicit base_packet(base_packet &other)
            : conn(other.conn),
              conversation_id(other.conversation_id),
              source_id(other.source_id),
              destination_id(other.destination_id) {
    }

    void set_comm_data(std::string conversation_id, int source_id, int destination_id) {
        this->conversation_id = std::move(conversation_id);
        this->source_id = source_id;
        this->destination_id = destination_id;
    }
};

// TCP packets
struct state_change_packet : base_packet
{
    int change_id;
    std::string new_state;

    state_change_packet(connection_ptr conn, int change_id, std::string new_state)
            : base_packet(conn),
              change_id(change_id),
              new_state(std::move(new_state)) {
    }
};

struct state_fetch_packet : base_packet
{
    int type_id;
    std::string data;

    explicit state_fetch_packet(connection_ptr conn, int type_id, std::string data)
            : base_packet(conn),
              type_id(type_id),
              data(std::move(data)) {
    }
};

struct state_response_packet : base_packet
{
    std::string state;

    state_response_packet(connection_ptr conn, std::string state)
            : base_packet(conn),
              state(std::move(state)) {
    }
};

struct control_packet : base_packet
{
    int change_id;
    std::string target_state;

    control_packet(connection_ptr conn, int change_id, std::string target_state)
            : base_packet(conn),
              change_id(change_id),
              target_state(std::move(target_state)) {
    }
};

struct keepalive_packet : base_packet
{
    explicit keepalive_packet(connection_ptr conn)
            : base_packet(conn) {
    }
};

using packet_attributes = boost::fusion::vector<std::string, int, int, int, int, boost::optional<std::string>>;

class decoded_packet
{
public:
    decoded_packet(std::string conversation_id, int source_id, int destination_id, int type, int instruction, boost::optional<std::string> data)
        : conversation_id(std::move(conversation_id)),
          source_id(source_id),
          destination_id(destination_id),
          type(type),
          instruction(instruction),
          data(data) {

    }

    std::string conversation_id;

    int source_id;
    int destination_id;

    int type;
    int instruction;

    boost::optional<std::string> data;
};

class message_handler
{
public:
    void handle(connection_ptr conn, std::string packet_source);

    std::string generate_response(connection_ptr conn, decoded_packet &packet);

    std::string static packet_to_string(state_change_packet const &packet);

    std::string static packet_to_string(state_fetch_packet const &packet);

    std::string static packet_to_string(state_response_packet const &packet);

    std::string static packet_to_string(control_packet const &packet);

    std::string static packet_to_string(keepalive_packet const &packet);

    decoded_packet static decode_packet(std::string const &packet);

protected:
    std::stringstream static base_packet_to_string(base_packet const &packet, int val);
};

#endif //SERVER_MESSAGE_HPP
