//
// Created by lex on 16-12-17.
//

#ifndef SERVER_ACTIONS_HPP
#define SERVER_ACTIONS_HPP

#include <iostream>

#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <boost/lexical_cast.hpp>

#include <message_handler.hpp>
#include <client_manager.hpp>
#include <controller_manager.hpp>
#include <message_handler.hpp>
#include <client_manager.hpp>
#include <controller_manager.hpp>
#include <app_manager.hpp>
#include <database.hpp>

class base_action
{
protected:
    std::string packet_source_string(base_packet const &packet) {
        std::stringstream ss;
        ss << "(src: " << packet.source_id << ", dst: " << packet.destination_id << ") -> ";
        return ss.str();
    }

    void print_packet(base_packet const &packet, std::string const &message) {
        std::cout << packet_source_string(packet) << message << std::endl;
    }
};

template<typename T>
class action : base_action, std::false_type
{
};

template<>
class action<state_change_packet> : base_action
{
public:
    explicit action(state_change_packet &packet)
            : packet_(packet) {
    }

    std::string make_response() {
        auto source = message_handler::packet_to_string(packet_);

        app_manager::get_instance().broadcast(source);
        controller_manager::get_instance().broadcast(source);

        database::get_instance().insert_event(
                event(packet_.change_id, packet_.source_id)
        );

        return source;
    }

    state_change_packet &packet_;
};

template<>
class action<state_fetch_packet> : base_action
{
public:
    explicit action(state_fetch_packet &packet)
            : packet_(packet) {
    }

    std::string make_response() {
        switch (packet_.type_id) {
            case state_fetches::get_clients: {
                std::stringstream ss;

                for (auto &client : client_manager::get_instance().get_clients()) {
                    ss << "i;" << client.id
                       << ';' << client.conn->getRemoteAddress().to_string()
                       << ",";
                }

                state_response_packet response_packet(packet_.conn, ss.str());

                response_packet.set_comm_data(packet_.conversation_id, packet_.source_id, packet_.destination_id);

                return message_handler::packet_to_string(response_packet);
            }

            case state_fetches::get_events: {
                std::vector<std::string> splitInputData;
                boost::split(splitInputData, packet_.data, boost::is_any_of(";"));

                auto amount = boost::lexical_cast<int>(splitInputData[0]);
                auto offset = boost::lexical_cast<int>(splitInputData[1]);

                std::stringstream responseData;
                for (auto &event : database::get_instance().get_events(amount, offset)) {
                    responseData << event.event_nr << ';'
                                 << event.event_type << ';'
                                 << event.room_nr << ',';
                }

                state_response_packet response_packet(packet_.conn, responseData.str());

                response_packet.set_comm_data(packet_.conversation_id, packet_.source_id, packet_.destination_id);

                return message_handler::packet_to_string(response_packet);
            }
            case state_fetches::get_ip: {
                auto id = boost::lexical_cast<int>(packet_.data);
                auto client = client_manager::get_instance().get_client(id);
                std::string ip;

                if (! client) {
                    ip = "";
                } else {
                    ip = (*client).conn->getRemoteAddress().to_string();
                }

                state_response_packet response_packet(packet_.conn, ip);
                response_packet.set_comm_data(packet_.conversation_id, packet_.source_id, packet_.destination_id);
                return message_handler::packet_to_string(response_packet);
            }
        }

        return "Unknown state change.";
    }

    state_fetch_packet &packet_;
};

template<>
class action<state_response_packet> : base_action
{
public:
    explicit action(state_response_packet &packet)
            : packet_(packet) {
    }

    std::string make_response() {
        return "state_response_packet";
    }

    state_response_packet &packet_;
};

template<>
class action<control_packet> : base_action
{
public:
    explicit action(control_packet &packet)
            : packet_(packet) {
    }

    std::string make_response() {
        switch (packet_.change_id) {
            case control_types::get_id: {
                packet_.target_state = std::to_string(
                        client_manager::get_instance().add_client(packet_.conn)
                );

                return message_handler::packet_to_string(packet_);
            }

            case control_types::register_controller: {
                controller_manager::get_instance().add_controller(packet_.conn);

                return message_handler::packet_to_string(packet_);
            }

            case control_types::register_app: {
                app_manager::get_instance().add_app(packet_.conn);

                return message_handler::packet_to_string(packet_);
            }

                // Pass the command on to the destinaton client.
            case control_types::turn_light_on:
            case control_types::turn_light_off: {
                std::string source = message_handler::packet_to_string(packet_);
                client_manager::get_instance().to_client(packet_.destination_id, source);

                return source;
            }

            default: {
                return "Unknown control packet type.";
            }
        }
    }

    control_packet &packet_;
};

template<>
class action<keepalive_packet> : base_action
{
public:
    explicit action(keepalive_packet &packet)
            : packet_(packet) {
    }

    std::string make_response() {
        return message_handler::packet_to_string(packet_);
    }

    keepalive_packet &packet_;
};


#endif //SERVER_ACTIONS_HPP
