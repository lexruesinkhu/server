//
// Created by lex on 29-1-18.
//

#ifndef SERVER_EVENTS_HPP
#define SERVER_EVENTS_HPP

struct event {
    int event_nr = -1;
    int event_type = -1;
    int room_nr = -1;

    event(int event_type, int room_nr)
        : event_type(event_type), room_nr(room_nr) { }

    event(int event_nr, int event_type, int room_nr)
            : event_nr(event_nr), event_type(event_type), room_nr(room_nr) { }
};

#endif //SERVER_EVENTS_HPP
