//
// Created by lex on 31-12-17.
//

#ifndef SERVER_CLIENT_MANAGER_HPP
#define SERVER_CLIENT_MANAGER_HPP

#include <string>
#include <utility>
#include <vector>

#include <boost/asio/ip/tcp.hpp>
#include <connection_manager.hpp>

using boost::asio::ip::tcp;

struct client
{
    connection_ptr conn;
    int id;

    client(int id, connection_ptr conn)
            : id(id), conn(conn) {
    }

    inline bool operator==(const connection_ptr& rhs) {
        return conn == rhs;
    }
};

class client_manager
{
public:
    /**
     * Add a client to the manager with the given ip address.
     * Will return the id of the client.
     *
     * If a client with that ip already exists, that id is returned.
     * Otherwise the first free id is returned.
     *
     * @param ip
     * @return
     */
    int add_client(connection_ptr conn);

    /**
     * Get a vector of all registered clients.
     *
     * @return
     */
    std::vector<client>& get_clients();

    /**
     * Get a reference to the client with
     * the given id.
     *
     * @param id
     * @return
     */
    boost::optional<client&> get_client(int id);

    /**
     * Send a message to all clients.
     *
     * @param message
     */
    void broadcast(std::string const &message);

    /**
     * Send a message to a specific client.
     *
     * @param id
     * @param message
     */
    void to_client(int id, std::string const &message);

    /**
     * Remove the given connection from the list if
     * it exists.
     *
     * @param conn
     */
    void remove_if_exists(connection_ptr conn);

    /**
     * Get the singleton instance of the client_manager class.
     *
     * @return
     */
    static client_manager &get_instance();

    client_manager(client_manager const &) = delete;

    void operator=(client_manager const &) = delete;

private:
    client_manager() = default;

protected:
    std::vector<client> clients_;
};


#endif //SERVER_CLIENT_MANAGER_HPP
