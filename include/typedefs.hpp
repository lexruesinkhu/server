//
// Created by lex on 23-1-18.
//

#ifndef SERVER_TYPEDEFS_HPP
#define SERVER_TYPEDEFS_HPP

#include <connection.hpp>

typedef std::shared_ptr<connection> connection_ptr;

#endif //SERVER_TYPEDEFS_HPP
