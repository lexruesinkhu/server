//
// Created by lex on 27-11-17.
//

#ifndef SERVER_DISCOVERY_HANDLER_HPP
#define SERVER_DISCOVERY_HANDLER_HPP

#include <boost/array.hpp>
#include <boost/asio.hpp>

using namespace boost;
using boost::asio::ip::udp;

class discovery_handler
{
public:
    explicit discovery_handler(asio::io_service &io_service, unsigned short port)
            : socket_(io_service, udp::endpoint(udp::v4(), port)) {
        start_receive();
    }

private:
    void start_receive();

    void handle_receive(const system::error_code& error_code, std::size_t);
    void handle_send(const system::error_code& error_code, std::size_t) {}

    udp::socket socket_;
    udp::endpoint remote_endpoint_;
    boost::array <char, 1> recv_buffer_{};
};


#endif //SERVER_DISCOVERY_HANDLER_HPP
