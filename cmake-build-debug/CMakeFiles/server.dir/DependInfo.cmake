# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/lex/Projects/HU/IDP/server/src/client_manager.cpp" "/home/lex/Projects/HU/IDP/server/cmake-build-debug/CMakeFiles/server.dir/src/client_manager.cpp.o"
  "/home/lex/Projects/HU/IDP/server/src/connection.cpp" "/home/lex/Projects/HU/IDP/server/cmake-build-debug/CMakeFiles/server.dir/src/connection.cpp.o"
  "/home/lex/Projects/HU/IDP/server/src/database.cpp" "/home/lex/Projects/HU/IDP/server/cmake-build-debug/CMakeFiles/server.dir/src/database.cpp.o"
  "/home/lex/Projects/HU/IDP/server/src/discovery_handler.cpp" "/home/lex/Projects/HU/IDP/server/cmake-build-debug/CMakeFiles/server.dir/src/discovery_handler.cpp.o"
  "/home/lex/Projects/HU/IDP/server/src/main.cpp" "/home/lex/Projects/HU/IDP/server/cmake-build-debug/CMakeFiles/server.dir/src/main.cpp.o"
  "/home/lex/Projects/HU/IDP/server/src/message_handler.cpp" "/home/lex/Projects/HU/IDP/server/cmake-build-debug/CMakeFiles/server.dir/src/message_handler.cpp.o"
  "/home/lex/Projects/HU/IDP/server/src/tcp_server.cpp" "/home/lex/Projects/HU/IDP/server/cmake-build-debug/CMakeFiles/server.dir/src/tcp_server.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../server"
  "../include"
  "../boost_1_66_0"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
