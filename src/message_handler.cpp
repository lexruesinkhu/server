//
// Created by lex on 25-11-17.
//

#include <message_handler.hpp>

#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>

#include <connection_manager.hpp>
#include <actions.hpp>

void message_handler::handle(connection_ptr conn, std::string packet_source) {
    auto packet = decode_packet(packet_source);
    auto response = generate_response(conn, packet);

    conn->write(response);
}

std::string message_handler::generate_response(connection_ptr conn, decoded_packet &packet) {
    std::string response;
    auto endpoint = conn->socket().remote_endpoint();

    switch (packet.type) {
        case packet_type::state_change: {
            state_change_packet scp(conn, packet.instruction, *packet.data);

            scp.set_comm_data(packet.conversation_id, packet.source_id, packet.destination_id);

            response = (action<state_change_packet>(scp)).make_response();
            break;
        }

        case packet_type::state_fetch: {
            state_fetch_packet sfp(conn, packet.instruction, *packet.data);

            sfp.set_comm_data(packet.conversation_id, packet.source_id, packet.destination_id);

            response = (action<state_fetch_packet>(sfp)).make_response();
            break;
        }

        case packet_type::state_response: {
            state_response_packet srp(conn, *packet.data);

            srp.set_comm_data(packet.conversation_id, packet.source_id, packet.destination_id);

            response = (action<state_response_packet>(srp)).make_response();
            break;
        }

        case packet_type::control: {
            control_packet cp(conn, packet.instruction, *packet.data);

            cp.set_comm_data(packet.conversation_id, packet.source_id, packet.destination_id);

            response = (action<control_packet>(cp)).make_response();

            break;
        }

        case packet_type::keepalive: {
            keepalive_packet kp(conn);

            kp.set_comm_data(packet.conversation_id, packet.source_id, packet.destination_id);
            response = (action<keepalive_packet>(kp)).make_response();
            break;
        }

        default: {
            //std::cout << "Got unkown packet from ip: " << endpoint.address().to_string() << std::endl;

            // Packet type was -1 because parsing failed
            response = "Unknown packet type.";
            break;
        }
    }

    return response;
}


std::string message_handler::packet_to_string(state_change_packet const& packet) {
    auto ss = base_packet_to_string(packet, packet_type::state_change);
    ss << ':' << packet.change_id << ':' << packet.new_state;
    return ss.str();
}

std::string message_handler::packet_to_string(state_fetch_packet const& packet) {
    auto ss = base_packet_to_string(packet, packet_type::state_fetch);
    ss << ':' << packet.type_id << ':' << packet.data;
    return ss.str();
}

std::string message_handler::packet_to_string(state_response_packet const& packet) {
    auto ss = base_packet_to_string(packet, packet_type::state_response);
    ss << ':' << 0 << ':' << packet.state;
    return ss.str();
}

std::string message_handler::packet_to_string(control_packet const& packet) {

    auto ss = base_packet_to_string(packet, packet_type::control);
    ss << ':' << packet.change_id << ':' << packet.target_state;
    return ss.str();
}

std::string message_handler::packet_to_string(keepalive_packet const& packet) {
    return base_packet_to_string(packet, packet_type::keepalive).str();
}

decoded_packet message_handler::decode_packet(std::string const& packet) {
    std::vector<std::string> parts;
    boost::split(parts, packet, boost::is_any_of(":"));

    std::string conversation_id = parts[0];
    auto source_id = boost::lexical_cast<int>(parts[1]);
    auto destination_id = boost::lexical_cast<int>(parts[2]);
    auto packet_type = boost::lexical_cast<int>(parts[3]);
    auto instruction = boost::lexical_cast<int>(parts[4]);

    boost::optional<std::string> data = boost::none;

    // There is data
    if (parts.size() == 6) {
        data = parts[5];
    }

    return decoded_packet(conversation_id, source_id, destination_id, packet_type, instruction, data);
}

std::stringstream message_handler::base_packet_to_string(base_packet const& packet, const int val) {
    std::stringstream ss;
    ss << packet.conversation_id
       << ':' << packet.source_id
       << ':' << packet.destination_id
       << ':' << val;

    return ss;
}