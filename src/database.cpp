//
// Created by lex on 15-1-18.
//

#include <database.hpp>

#include <sqlite3.h>
#include <iostream>
#include <sstream>

database &database::get_instance() {
    static database db;

    return db;
}

bool database::init(const char *filename) {
    std::cout << "Trying to open database from " << filename << std::endl;
    int rc = sqlite3_open(filename, &db);

    if (rc) {
        std::cout << "Can't open database: " << sqlite3_errmsg(db) << std::endl;
        return false;
    }

    if (needs_setup()) {
        bootstrap();
    }

    std::cout << "Database opened." << std::endl;
    return true;
}

void database::close() {
    sqlite3_close(db);
    std::cout << "Database closed." << std::endl;
}

const char* database::query(std::string const &query) {
    sqlite3_stmt *statement;

    if (sqlite3_prepare_v2(db, query.c_str(), -1, &statement, nullptr) != SQLITE_OK) {
        throw std::runtime_error(sqlite3_errmsg(db));
    }

    if (sqlite3_step(statement) == SQLITE_ERROR) {
        throw std::runtime_error(sqlite3_errmsg(db));
    }

    auto column = sqlite3_column_text(statement, 0);
    auto result = reinterpret_cast<const char*>(column);

    return result;
}

void database::insert_event(event event) {
    std::stringstream ss;
    ss << "INSERT INTO `event` (`event_type`, `room_nr`) VALUES("
       << event.event_type << ','
       << event.room_nr << ");";

    query(ss.str());
}

std::vector<event> database::get_events(int amount, int offset) {
    sqlite3_stmt *statement;

    std::stringstream ss;
    ss << "SELECT * FROM event LIMIT " << amount;

    if (offset > 0) {
        ss << " OFFSET " << offset;
    }

    auto query = ss.str();

    if (sqlite3_prepare_v2(db, query.c_str(), -1, &statement, nullptr) != SQLITE_OK) {
        std::cout << "Error executing query: " << query
                  <<  ", error: " << sqlite3_errmsg(db)
                  << std::endl;
        throw std::runtime_error("Error retrieving events from database.");
    }

    std::vector<event> result_set;

    while (sqlite3_step(statement) == SQLITE_ROW) {
        int event_nr = sqlite3_column_int(statement, 0);
        int event_type = sqlite3_column_int(statement, 1);
        int room_nr = sqlite3_column_int(statement, 2);

        result_set.emplace_back(event_nr, event_type, room_nr);
    }

    int rc = sqlite3_step(statement);

    if (rc != SQLITE_DONE && rc != SQLITE_ROW) {
        sqlite3_finalize(statement);
        throw std::runtime_error(sqlite3_errmsg(db));
    }

    return result_set;
}

void database::clear_events() {
    query("DELETE FROM event");
}

void database::bootstrap() {
    const char *query = "CREATE TABLE event("
            "event_nr INTEGER PRIMARY KEY NOT NULL,"
            "event_type INTEGER NOT NULL,"
            "room_nr INTEGER NOT NULL"
            ");";

    sqlite3_exec(db, query, nullptr, nullptr, nullptr);

    std::cout << "Created tables in database." << std::endl;
}


bool database::needs_setup() {
    std::string q = "SELECT name FROM sqlite_master WHERE type='table' AND name='event';";

    auto result =  query(q);

    return result == nullptr || result == "NULL";
}