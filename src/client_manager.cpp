//
// Created by lex on 31-12-17.
//

#include <client_manager.hpp>

int client_manager::add_client(connection_ptr conn) {
    auto endpoint = conn->socket().remote_endpoint();

    std::cout << "Got registration request from " << endpoint.address().to_string() << std::endl;

    auto result = std::find_if(std::begin(clients_), std::end(clients_), [&](client const &c) {
        /*
         * Source ports may differ on these endpoints, so we only want to
         * compare their addresses.
         */
        return c.conn->socket().remote_endpoint().address() == endpoint.address();
    });


    if (result != std::end(clients_)) {
        std::cout << "Client already registered." << std::endl;

        // Update the socket on the client
        result->conn = conn;

        // Client already exists
        return result->id;
    }

    int id = 1;

    // New id needs to be generated
    // TODO: this doesn't reuse ids!
    if (!clients_.empty()) {
        id = clients_.back().id + 1;
    }

    clients_.emplace_back(id, conn);

    std::cout << "\nClient listing:\n";

    for (auto &client : clients_) {
        std::cout << "\t " << client.id << " -> " << client.conn->socket().remote_endpoint().address().to_string()
                  << "\n";
    }

    std::cout << std::endl;

    return id;
}

std::vector<client> &client_manager::get_clients() {
    return clients_;
}

boost::optional<client&> client_manager::get_client(int id) {
    boost::optional<client&> value = boost::none;

    for (auto& client : clients_) {
        if (client.id == id) {
            value = client;
            break;
        }
    }

    return value;
}


void client_manager::broadcast(const std::string &message) {
    for (auto& client : clients_) {
        client.conn->write(message);
    }
}

void client_manager::to_client(int id, const std::string &message) {
    for (auto& client : clients_) {
        if (client.id == id) {
            client.conn->write(message);
            return;
        }
    }

    assert(false);
}

client_manager &client_manager::get_instance() {
    static client_manager cm;

    return cm;
}

void client_manager::remove_if_exists(connection_ptr conn) {
    auto it = std::find(std::begin(clients_), std::end(clients_), conn);

    if (it != std::end(clients_)) {
        clients_.erase(it);
    }
}