//
// Created by lex on 25-11-17.
//

#include <tcp_server.hpp>

tcp_server::tcp_server(asio::io_service &io_service, unsigned short port)
        : io_service_(io_service),
          acceptor_(io_service, tcp::endpoint(tcp::v4(), port)) {

    do_accept();
}

void tcp_server::do_accept() {
    auto new_conn = connection_manager::get_instance().make_new(io_service_);

    acceptor_.async_accept(new_conn->socket(),
                           boost::bind(
                                   &tcp_server::handle_accept,
                                   this, new_conn, asio::placeholders::error
                           ));
}

void tcp_server::handle_accept(connection_ptr conn, boost::system::error_code const &ec) {
    if (!ec) {
        connection_manager::get_instance().start_reading(conn);
    } else {
        std::cout << "Error accepting incoming connection: " << ec.message() << std::endl;
    }

    do_accept();
}
