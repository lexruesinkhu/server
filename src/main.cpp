#include <thread>
#include <iostream>
#include <vector>

#include <boost/asio.hpp>

#include <tcp_server.hpp>
#include <discovery_handler.hpp>
#include <database.hpp>
#include <cli.hpp>

int main() {
    auto &db = database::get_instance();

    if (!db.init("events.sqlite")) {
        return 1;
    }

    try {
        boost::asio::io_service io_service;

        std::cout << "Starting tcp server on port 2500.\n";
        tcp_server server(io_service, 2500);

        std::cout << "Starting udp discovery listener on port 2501\n";
        discovery_handler handler(io_service, 2501);

        auto thread_count = std::thread::hardware_concurrency();

        // Leave at least 2 extra cores.
        if (thread_count > 2) {
            thread_count--;
        }

        std::cout << "Creating " << thread_count << " worker threads." << std::endl;

        std::vector<std::thread> threads;

        for (int i = 0; i < thread_count; i++) {
            threads.emplace_back([&io_service]() { io_service.run(); });
        }

        cli interface;

        interface.loop();

        io_service.stop();

        // TODO: stop listening somehow, right now join never happens because there is alwasy work in the io service
        for (auto &thread : threads) {
            thread.join();
        }
    } catch (std::exception &e) {
        std::cout << "Exception: " << e.what() << "\n";
    }

    std::cout << "Server done.\n";

    return 0;
}
