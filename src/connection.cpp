//
// Created by lex on 23-1-18.
//

#include <connection.hpp>
#include <connection_manager.hpp>
#include <client_manager.hpp>
#include <app_manager.hpp>
#include <controller_manager.hpp>

void connection::read(connection::receive_handler on_receive) {
    do_read(on_receive);

    std::cout << "Started listening on new connection." << std::endl;
}

void connection::write(const std::string &message) {
    bool write_in_progress = !write_queue_.empty();
    write_queue_.push_back(message);

    if (!write_in_progress) {
        handle_write();
    }
}


void connection::do_read(connection::receive_handler on_receive, const boost::system::error_code &ec) {
    if (!ec) {
        socket_.async_read_some(boost::asio::buffer(read_buffer_, buffer_length),
                                boost::bind(&connection::handle_read, shared_from_this(), on_receive,
                                            boost::asio::placeholders::error,
                                            boost::asio::placeholders::bytes_transferred));
    } else {
        std::cerr << "do_read: " << ec.message() << std::endl;
    }
}


void connection::handle_read(receive_handler on_receive, boost::system::error_code const &ec, size_t bytes_received) {
    if (!ec) {
        on_receive(shared_from_this(), std::string(read_buffer_, bytes_received));
        do_read(on_receive);
    } else {
        if (ec == boost::asio::error::eof) {
            // Connection closed by peer, clean up connection.
        } else {
            // Actual error, log to console.
            std::cerr << "handle_read: " << ec.message() << std::endl;
        }

        client_manager::get_instance().remove_if_exists(shared_from_this());
        app_manager::get_instance().remove_if_exists(shared_from_this());
        controller_manager::get_instance().remove_if_exists(shared_from_this());


        connection_manager::get_instance().remove(shared_from_this());
    }
}

void connection::handle_write() {
    auto data = write_queue_.front().data();
    auto length = write_queue_.front().length();

    auto lambda = [this](boost::system::error_code const &ec, std::size_t /* length */) {
        if (!ec) {
            write_queue_.pop_front();

            if (!write_queue_.empty()) {
                handle_write();
            }
        }
    };

    boost::asio::async_write(socket_, boost::asio::buffer(data, length), lambda);
}
