//
// Created by lex on 27-11-17.
//

#include <discovery_handler.hpp>

#include <string>
#include <iostream>

#include <boost/bind.hpp>

#include <message_handler.hpp>

void discovery_handler::start_receive() {
    socket_.async_receive_from(
            asio::buffer(recv_buffer_), remote_endpoint_,
            boost::bind(&discovery_handler::handle_receive, this,
                        asio::placeholders::error,
                        asio::placeholders::bytes_transferred));
}

void discovery_handler::handle_receive(const system::error_code &error_code, std::size_t) {
    if (!error_code || error_code == asio::error::message_size) {
        std::cout << "Received discovery packet from " << remote_endpoint_.address()  << std::endl;

        socket_.async_send_to(asio::buffer(""), remote_endpoint_,
                boost::bind(&discovery_handler::handle_send, this,
                         asio::placeholders::error,
                         asio::placeholders::bytes_transferred));

        start_receive();
    }
}